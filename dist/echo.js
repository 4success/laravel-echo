function _typeof(o) {
  "@babel/helpers - typeof";

  return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
    return typeof o;
  } : function (o) {
    return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
  }, _typeof(o);
}
function _extends() {
  _extends = Object.assign ? Object.assign.bind() : function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  return _extends.apply(this, arguments);
}

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

/**
 * This class represents a basic channel.
 */
var Channel = /** @class */function () {
  function Channel() {}
  /**
   * Listen for a whisper event on the channel instance.
   */
  Channel.prototype.listenForWhisper = function (event, callback) {
    return this.listen('.client-' + event, callback);
  };
  /**
   * Listen for an event on the channel instance.
   */
  Channel.prototype.notification = function (callback) {
    return this.listen('.Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', callback);
  };
  /**
   * Stop listening for a whisper event on the channel instance.
   */
  Channel.prototype.stopListeningForWhisper = function (event, callback) {
    return this.stopListening('.client-' + event, callback);
  };
  return Channel;
}();

/**
 * Event name formatter
 */
var EventFormatter = /** @class */function () {
  /**
   * Create a new class instance.
   */
  function EventFormatter(namespace) {
    this.namespace = namespace;
    //
  }
  /**
   * Format the given event name.
   */
  EventFormatter.prototype.format = function (event) {
    if (event.charAt(0) === '.' || event.charAt(0) === '\\') {
      return event.substr(1);
    } else if (this.namespace) {
      event = this.namespace + '.' + event;
    }
    return event.replace(/\./g, '\\');
  };
  /**
   * Set the event namespace.
   */
  EventFormatter.prototype.setNamespace = function (value) {
    this.namespace = value;
  };
  return EventFormatter;
}();

/**
 * This class represents a Pusher channel.
 */
var PusherChannel = /** @class */function (_super) {
  __extends(PusherChannel, _super);
  /**
   * Create a new class instance.
   */
  function PusherChannel(pusher, name, options) {
    var _this = _super.call(this) || this;
    _this.name = name;
    _this.pusher = pusher;
    _this.options = options;
    _this.eventFormatter = new EventFormatter(_this.options.namespace);
    _this.subscribe();
    return _this;
  }
  /**
   * Subscribe to a Pusher channel.
   */
  PusherChannel.prototype.subscribe = function () {
    this.subscription = this.pusher.subscribe(this.name);
  };
  /**
   * Unsubscribe from a Pusher channel.
   */
  PusherChannel.prototype.unsubscribe = function () {
    this.pusher.unsubscribe(this.name);
  };
  /**
   * Listen for an event on the channel instance.
   */
  PusherChannel.prototype.listen = function (event, callback) {
    this.on(this.eventFormatter.format(event), callback);
    return this;
  };
  /**
   * Listen for all events on the channel instance.
   */
  PusherChannel.prototype.listenToAll = function (callback) {
    var _this = this;
    this.subscription.bind_global(function (event, data) {
      if (event.startsWith('pusher:')) {
        return;
      }
      var namespace = _this.options.namespace.replace(/\./g, '\\');
      var formattedEvent = event.startsWith(namespace) ? event.substring(namespace.length + 1) : '.' + event;
      callback(formattedEvent, data);
    });
    return this;
  };
  /**
   * Stop listening for an event on the channel instance.
   */
  PusherChannel.prototype.stopListening = function (event, callback) {
    if (callback) {
      this.subscription.unbind(this.eventFormatter.format(event), callback);
    } else {
      this.subscription.unbind(this.eventFormatter.format(event));
    }
    return this;
  };
  /**
   * Stop listening for all events on the channel instance.
   */
  PusherChannel.prototype.stopListeningToAll = function (callback) {
    if (callback) {
      this.subscription.unbind_global(callback);
    } else {
      this.subscription.unbind_global();
    }
    return this;
  };
  /**
   * Register a callback to be called anytime a subscription succeeds.
   */
  PusherChannel.prototype.subscribed = function (callback) {
    this.on('pusher:subscription_succeeded', function () {
      callback();
    });
    return this;
  };
  /**
   * Register a callback to be called anytime a subscription error occurs.
   */
  PusherChannel.prototype.error = function (callback) {
    this.on('pusher:subscription_error', function (status) {
      callback(status);
    });
    return this;
  };
  /**
   * Bind a channel to an event.
   */
  PusherChannel.prototype.on = function (event, callback) {
    this.subscription.bind(event, callback);
    return this;
  };
  return PusherChannel;
}(Channel);

/**
 * This class represents a Pusher private channel.
 */
var PusherPrivateChannel = /** @class */function (_super) {
  __extends(PusherPrivateChannel, _super);
  function PusherPrivateChannel() {
    return _super !== null && _super.apply(this, arguments) || this;
  }
  /**
   * Send a whisper event to other clients in the channel.
   */
  PusherPrivateChannel.prototype.whisper = function (eventName, data) {
    this.pusher.channels.channels[this.name].trigger("client-".concat(eventName), data);
    return this;
  };
  return PusherPrivateChannel;
}(PusherChannel);

/**
 * This class represents a Pusher private channel.
 */
var PusherEncryptedPrivateChannel = /** @class */function (_super) {
  __extends(PusherEncryptedPrivateChannel, _super);
  function PusherEncryptedPrivateChannel() {
    return _super !== null && _super.apply(this, arguments) || this;
  }
  /**
   * Send a whisper event to other clients in the channel.
   */
  PusherEncryptedPrivateChannel.prototype.whisper = function (eventName, data) {
    this.pusher.channels.channels[this.name].trigger("client-".concat(eventName), data);
    return this;
  };
  return PusherEncryptedPrivateChannel;
}(PusherChannel);

/**
 * This class represents a Pusher presence channel.
 */
var PusherPresenceChannel = /** @class */function (_super) {
  __extends(PusherPresenceChannel, _super);
  function PusherPresenceChannel() {
    return _super !== null && _super.apply(this, arguments) || this;
  }
  /**
   * Register a callback to be called anytime the member list changes.
   */
  PusherPresenceChannel.prototype.here = function (callback) {
    this.on('pusher:subscription_succeeded', function (data) {
      callback(Object.keys(data.members).map(function (k) {
        return data.members[k];
      }));
    });
    return this;
  };
  /**
   * Listen for someone joining the channel.
   */
  PusherPresenceChannel.prototype.joining = function (callback) {
    this.on('pusher:member_added', function (member) {
      callback(member.info);
    });
    return this;
  };
  /**
   * Send a whisper event to other clients in the channel.
   */
  PusherPresenceChannel.prototype.whisper = function (eventName, data) {
    this.pusher.channels.channels[this.name].trigger("client-".concat(eventName), data);
    return this;
  };
  /**
   * Listen for someone leaving the channel.
   */
  PusherPresenceChannel.prototype.leaving = function (callback) {
    this.on('pusher:member_removed', function (member) {
      callback(member.info);
    });
    return this;
  };
  return PusherPresenceChannel;
}(PusherChannel);

/**
 * This class represents a Socket.io channel.
 */
var SocketIoChannel = /** @class */function (_super) {
  __extends(SocketIoChannel, _super);
  /**
   * Create a new class instance.
   */
  function SocketIoChannel(socket, name, options) {
    var _this = _super.call(this) || this;
    /**
     * The event callbacks applied to the socket.
     */
    _this.events = {};
    /**
     * User supplied callbacks for events on this channel.
     */
    _this.listeners = {};
    _this.name = name;
    _this.socket = socket;
    _this.options = options;
    _this.eventFormatter = new EventFormatter(_this.options.namespace);
    _this.subscribe();
    return _this;
  }
  /**
   * Subscribe to a Socket.io channel.
   */
  SocketIoChannel.prototype.subscribe = function () {
    this.socket.emit('subscribe', {
      channel: this.name,
      auth: this.options.auth || {}
    });
  };
  /**
   * Unsubscribe from channel and ubind event callbacks.
   */
  SocketIoChannel.prototype.unsubscribe = function () {
    this.unbind();
    this.socket.emit('unsubscribe', {
      channel: this.name,
      auth: this.options.auth || {}
    });
  };
  /**
   * Listen for an event on the channel instance.
   */
  SocketIoChannel.prototype.listen = function (event, callback) {
    this.on(this.eventFormatter.format(event), callback);
    return this;
  };
  /**
   * Stop listening for an event on the channel instance.
   */
  SocketIoChannel.prototype.stopListening = function (event, callback) {
    this.unbindEvent(this.eventFormatter.format(event), callback);
    return this;
  };
  /**
   * Register a callback to be called anytime a subscription succeeds.
   */
  SocketIoChannel.prototype.subscribed = function (callback) {
    this.on('connect', function (socket) {
      callback(socket);
    });
    return this;
  };
  /**
   * Register a callback to be called anytime an error occurs.
   */
  SocketIoChannel.prototype.error = function (callback) {
    return this;
  };
  /**
   * Bind the channel's socket to an event and store the callback.
   */
  SocketIoChannel.prototype.on = function (event, callback) {
    var _this = this;
    this.listeners[event] = this.listeners[event] || [];
    if (!this.events[event]) {
      this.events[event] = function (channel, data) {
        if (_this.name === channel && _this.listeners[event]) {
          _this.listeners[event].forEach(function (cb) {
            return cb(data);
          });
        }
      };
      this.socket.on(event, this.events[event]);
    }
    this.listeners[event].push(callback);
    return this;
  };
  /**
   * Unbind the channel's socket from all stored event callbacks.
   */
  SocketIoChannel.prototype.unbind = function () {
    var _this = this;
    Object.keys(this.events).forEach(function (event) {
      _this.unbindEvent(event);
    });
  };
  /**
   * Unbind the listeners for the given event.
   */
  SocketIoChannel.prototype.unbindEvent = function (event, callback) {
    this.listeners[event] = this.listeners[event] || [];
    if (callback) {
      this.listeners[event] = this.listeners[event].filter(function (cb) {
        return cb !== callback;
      });
    }
    if (!callback || this.listeners[event].length === 0) {
      if (this.events[event]) {
        this.socket.removeListener(event, this.events[event]);
        delete this.events[event];
      }
      delete this.listeners[event];
    }
  };
  return SocketIoChannel;
}(Channel);

/**
 * This class represents a Socket.io private channel.
 */
var SocketIoPrivateChannel = /** @class */function (_super) {
  __extends(SocketIoPrivateChannel, _super);
  function SocketIoPrivateChannel() {
    return _super !== null && _super.apply(this, arguments) || this;
  }
  /**
   * Send a whisper event to other clients in the channel.
   */
  SocketIoPrivateChannel.prototype.whisper = function (eventName, data) {
    this.socket.emit('client event', {
      channel: this.name,
      event: "client-".concat(eventName),
      data: data
    });
    return this;
  };
  return SocketIoPrivateChannel;
}(SocketIoChannel);

/**
 * This class represents a Socket.io presence channel.
 */
var SocketIoPresenceChannel = /** @class */function (_super) {
  __extends(SocketIoPresenceChannel, _super);
  function SocketIoPresenceChannel() {
    return _super !== null && _super.apply(this, arguments) || this;
  }
  /**
   * Register a callback to be called anytime the member list changes.
   */
  SocketIoPresenceChannel.prototype.here = function (callback) {
    this.on('presence:subscribed', function (members) {
      callback(members.map(function (m) {
        return m.user_info;
      }));
    });
    return this;
  };
  /**
   * Listen for someone joining the channel.
   */
  SocketIoPresenceChannel.prototype.joining = function (callback) {
    this.on('presence:joining', function (member) {
      return callback(member.user_info);
    });
    return this;
  };
  /**
   * Send a whisper event to other clients in the channel.
   */
  SocketIoPresenceChannel.prototype.whisper = function (eventName, data) {
    this.socket.emit('client event', {
      channel: this.name,
      event: "client-".concat(eventName),
      data: data
    });
    return this;
  };
  /**
   * Listen for someone leaving the channel.
   */
  SocketIoPresenceChannel.prototype.leaving = function (callback) {
    this.on('presence:leaving', function (member) {
      return callback(member.user_info);
    });
    return this;
  };
  return SocketIoPresenceChannel;
}(SocketIoPrivateChannel);

/**
 * This class represents a null channel.
 */
var NullChannel = /** @class */function (_super) {
  __extends(NullChannel, _super);
  function NullChannel() {
    return _super !== null && _super.apply(this, arguments) || this;
  }
  /**
   * Subscribe to a channel.
   */
  NullChannel.prototype.subscribe = function () {
    //
  };
  /**
   * Unsubscribe from a channel.
   */
  NullChannel.prototype.unsubscribe = function () {
    //
  };
  /**
   * Listen for an event on the channel instance.
   */
  NullChannel.prototype.listen = function (event, callback) {
    return this;
  };
  /**
   * Listen for all events on the channel instance.
   */
  NullChannel.prototype.listenToAll = function (callback) {
    return this;
  };
  /**
   * Stop listening for an event on the channel instance.
   */
  NullChannel.prototype.stopListening = function (event, callback) {
    return this;
  };
  /**
   * Register a callback to be called anytime a subscription succeeds.
   */
  NullChannel.prototype.subscribed = function (callback) {
    return this;
  };
  /**
   * Register a callback to be called anytime an error occurs.
   */
  NullChannel.prototype.error = function (callback) {
    return this;
  };
  /**
   * Bind a channel to an event.
   */
  NullChannel.prototype.on = function (event, callback) {
    return this;
  };
  return NullChannel;
}(Channel);

/**
 * This class represents a null private channel.
 */
var NullPrivateChannel = /** @class */function (_super) {
  __extends(NullPrivateChannel, _super);
  function NullPrivateChannel() {
    return _super !== null && _super.apply(this, arguments) || this;
  }
  /**
   * Send a whisper event to other clients in the channel.
   */
  NullPrivateChannel.prototype.whisper = function (eventName, data) {
    return this;
  };
  return NullPrivateChannel;
}(NullChannel);

/**
 * This class represents a null presence channel.
 */
var NullPresenceChannel = /** @class */function (_super) {
  __extends(NullPresenceChannel, _super);
  function NullPresenceChannel() {
    return _super !== null && _super.apply(this, arguments) || this;
  }
  /**
   * Register a callback to be called anytime the member list changes.
   */
  NullPresenceChannel.prototype.here = function (callback) {
    return this;
  };
  /**
   * Listen for someone joining the channel.
   */
  NullPresenceChannel.prototype.joining = function (callback) {
    return this;
  };
  /**
   * Send a whisper event to other clients in the channel.
   */
  NullPresenceChannel.prototype.whisper = function (eventName, data) {
    return this;
  };
  /**
   * Listen for someone leaving the channel.
   */
  NullPresenceChannel.prototype.leaving = function (callback) {
    return this;
  };
  return NullPresenceChannel;
}(NullChannel);

var Connector = /** @class */function () {
  /**
   * Create a new class instance.
   */
  function Connector(options) {
    /**
     * Default connector options.
     */
    this._defaultOptions = {
      auth: {
        headers: {}
      },
      authEndpoint: '/broadcasting/auth',
      userAuthentication: {
        endpoint: '/broadcasting/user-auth',
        headers: {}
      },
      broadcaster: 'pusher',
      csrfToken: null,
      bearerToken: null,
      host: null,
      key: null,
      namespace: 'App.Events'
    };
    this.setOptions(options);
    this.connect();
  }
  /**
   * Merge the custom options with the defaults.
   */
  Connector.prototype.setOptions = function (options) {
    this.options = _extends(this._defaultOptions, options);
    var token = this.csrfToken();
    if (token) {
      this.options.auth.headers['X-CSRF-TOKEN'] = token;
      this.options.userAuthentication.headers['X-CSRF-TOKEN'] = token;
    }
    token = this.options.bearerToken;
    if (token) {
      this.options.auth.headers['Authorization'] = 'Bearer ' + token;
      this.options.userAuthentication.headers['Authorization'] = 'Bearer ' + token;
    }
    return options;
  };
  /**
   * Extract the CSRF token from the page.
   */
  Connector.prototype.csrfToken = function () {
    var selector;
    if (typeof window !== 'undefined' && window['Laravel'] && window['Laravel'].csrfToken) {
      return window['Laravel'].csrfToken;
    } else if (this.options.csrfToken) {
      return this.options.csrfToken;
    } else if (typeof document !== 'undefined' && typeof document.querySelector === 'function' && (selector = document.querySelector('meta[name="csrf-token"]'))) {
      return selector.getAttribute('content');
    }
    return null;
  };
  return Connector;
}();

/**
 * This class creates a connector to Pusher.
 */
var PusherConnector = /** @class */function (_super) {
  __extends(PusherConnector, _super);
  function PusherConnector() {
    var _this = _super !== null && _super.apply(this, arguments) || this;
    /**
     * All of the subscribed channel names.
     */
    _this.channels = {};
    return _this;
  }
  /**
   * Create a fresh Pusher connection.
   */
  PusherConnector.prototype.connect = function () {
    if (typeof this.options.client !== 'undefined') {
      this.pusher = this.options.client;
    } else if (this.options.Pusher) {
      this.pusher = new this.options.Pusher(this.options.key, this.options);
    } else {
      this.pusher = new Pusher(this.options.key, this.options);
    }
  };
  /**
   * Sign in the user via Pusher user authentication (https://pusher.com/docs/channels/using_channels/user-authentication/).
   */
  PusherConnector.prototype.signin = function () {
    this.pusher.signin();
  };
  /**
   * Listen for an event on a channel instance.
   */
  PusherConnector.prototype.listen = function (name, event, callback) {
    return this.channel(name).listen(event, callback);
  };
  /**
   * Get a channel instance by name.
   */
  PusherConnector.prototype.channel = function (name) {
    if (!this.channels[name]) {
      this.channels[name] = new PusherChannel(this.pusher, name, this.options);
    }
    return this.channels[name];
  };
  /**
   * Get a private channel instance by name.
   */
  PusherConnector.prototype.privateChannel = function (name) {
    if (!this.channels['private-' + name]) {
      this.channels['private-' + name] = new PusherPrivateChannel(this.pusher, 'private-' + name, this.options);
    }
    return this.channels['private-' + name];
  };
  /**
   * Get a private encrypted channel instance by name.
   */
  PusherConnector.prototype.encryptedPrivateChannel = function (name) {
    if (!this.channels['private-encrypted-' + name]) {
      this.channels['private-encrypted-' + name] = new PusherEncryptedPrivateChannel(this.pusher, 'private-encrypted-' + name, this.options);
    }
    return this.channels['private-encrypted-' + name];
  };
  /**
   * Get a presence channel instance by name.
   */
  PusherConnector.prototype.presenceChannel = function (name) {
    if (!this.channels['presence-' + name]) {
      this.channels['presence-' + name] = new PusherPresenceChannel(this.pusher, 'presence-' + name, this.options);
    }
    return this.channels['presence-' + name];
  };
  /**
   * Leave the given channel, as well as its private and presence variants.
   */
  PusherConnector.prototype.leave = function (name) {
    var _this = this;
    var channels = [name, 'private-' + name, 'private-encrypted-' + name, 'presence-' + name];
    channels.forEach(function (name, index) {
      _this.leaveChannel(name);
    });
  };
  /**
   * Leave the given channel.
   */
  PusherConnector.prototype.leaveChannel = function (name) {
    if (this.channels[name]) {
      this.channels[name].unsubscribe();
      delete this.channels[name];
    }
  };
  /**
   * Get the socket ID for the connection.
   */
  PusherConnector.prototype.socketId = function () {
    return this.pusher.connection.socket_id;
  };
  /**
   * Disconnect Pusher connection.
   */
  PusherConnector.prototype.disconnect = function () {
    this.pusher.disconnect();
  };
  return PusherConnector;
}(Connector);

/**
 * This class creates a connnector to a Socket.io server.
 */
var SocketIoConnector = /** @class */function (_super) {
  __extends(SocketIoConnector, _super);
  function SocketIoConnector() {
    var _this = _super !== null && _super.apply(this, arguments) || this;
    /**
     * All of the subscribed channel names.
     */
    _this.channels = {};
    return _this;
  }
  /**
   * Create a fresh Socket.io connection.
   */
  SocketIoConnector.prototype.connect = function () {
    var _this = this;
    var io = this.getSocketIO();
    this.socket = io(this.options.host, this.options);
    this.socket.on('reconnect', function () {
      Object.values(_this.channels).forEach(function (channel) {
        channel.subscribe();
      });
    });
    return this.socket;
  };
  /**
   * Get socket.io module from global scope or options.
   */
  SocketIoConnector.prototype.getSocketIO = function () {
    if (typeof this.options.client !== 'undefined') {
      return this.options.client;
    }
    if (typeof io !== 'undefined') {
      return io;
    }
    throw new Error('Socket.io client not found. Should be globally available or passed via options.client');
  };
  /**
   * Listen for an event on a channel instance.
   */
  SocketIoConnector.prototype.listen = function (name, event, callback) {
    return this.channel(name).listen(event, callback);
  };
  /**
   * Get a channel instance by name.
   */
  SocketIoConnector.prototype.channel = function (name) {
    if (!this.channels[name]) {
      this.channels[name] = new SocketIoChannel(this.socket, name, this.options);
    }
    return this.channels[name];
  };
  /**
   * Get a private channel instance by name.
   */
  SocketIoConnector.prototype.privateChannel = function (name) {
    if (!this.channels['private-' + name]) {
      this.channels['private-' + name] = new SocketIoPrivateChannel(this.socket, 'private-' + name, this.options);
    }
    return this.channels['private-' + name];
  };
  /**
   * Get a presence channel instance by name.
   */
  SocketIoConnector.prototype.presenceChannel = function (name) {
    if (!this.channels['presence-' + name]) {
      this.channels['presence-' + name] = new SocketIoPresenceChannel(this.socket, 'presence-' + name, this.options);
    }
    return this.channels['presence-' + name];
  };
  /**
   * Leave the given channel, as well as its private and presence variants.
   */
  SocketIoConnector.prototype.leave = function (name) {
    var _this = this;
    var channels = [name, 'private-' + name, 'presence-' + name];
    channels.forEach(function (name) {
      _this.leaveChannel(name);
    });
  };
  /**
   * Leave the given channel.
   */
  SocketIoConnector.prototype.leaveChannel = function (name) {
    if (this.channels[name]) {
      this.channels[name].unsubscribe();
      delete this.channels[name];
    }
  };
  /**
   * Get the socket ID for the connection.
   */
  SocketIoConnector.prototype.socketId = function () {
    return this.socket.id;
  };
  /**
   * Disconnect Socketio connection.
   */
  SocketIoConnector.prototype.disconnect = function () {
    this.socket.disconnect();
  };
  return SocketIoConnector;
}(Connector);

/**
 * This class creates a null connector.
 */
var NullConnector = /** @class */function (_super) {
  __extends(NullConnector, _super);
  function NullConnector() {
    var _this = _super !== null && _super.apply(this, arguments) || this;
    /**
     * All of the subscribed channel names.
     */
    _this.channels = {};
    return _this;
  }
  /**
   * Create a fresh connection.
   */
  NullConnector.prototype.connect = function () {
    //
  };
  /**
   * Listen for an event on a channel instance.
   */
  NullConnector.prototype.listen = function (name, event, callback) {
    return new NullChannel();
  };
  /**
   * Get a channel instance by name.
   */
  NullConnector.prototype.channel = function (name) {
    return new NullChannel();
  };
  /**
   * Get a private channel instance by name.
   */
  NullConnector.prototype.privateChannel = function (name) {
    return new NullPrivateChannel();
  };
  /**
   * Get a private encrypted channel instance by name.
   */
  NullConnector.prototype.encryptedPrivateChannel = function (name) {
    return new NullPrivateChannel();
  };
  /**
   * Get a presence channel instance by name.
   */
  NullConnector.prototype.presenceChannel = function (name) {
    return new NullPresenceChannel();
  };
  /**
   * Leave the given channel, as well as its private and presence variants.
   */
  NullConnector.prototype.leave = function (name) {
    //
  };
  /**
   * Leave the given channel.
   */
  NullConnector.prototype.leaveChannel = function (name) {
    //
  };
  /**
   * Get the socket ID for the connection.
   */
  NullConnector.prototype.socketId = function () {
    return 'fake-socket-id';
  };
  /**
   * Disconnect the connection.
   */
  NullConnector.prototype.disconnect = function () {
    //
  };
  return NullConnector;
}(Connector);

/**
 * This class is the primary API for interacting with broadcasting.
 */
var Echo = /** @class */function () {
  /**
   * Create a new class instance.
   */
  function Echo(options) {
    this.options = options;
    this.connect();
    if (!this.options.withoutInterceptors) {
      this.registerInterceptors();
    }
  }
  /**
   * Get a channel instance by name.
   */
  Echo.prototype.channel = function (channel) {
    return this.connector.channel(channel);
  };
  /**
   * Create a new connection.
   */
  Echo.prototype.connect = function () {
    if (this.options.broadcaster == 'reverb') {
      this.connector = new PusherConnector(__assign(__assign({}, this.options), {
        cluster: ''
      }));
    } else if (this.options.broadcaster == 'pusher') {
      this.connector = new PusherConnector(this.options);
    } else if (this.options.broadcaster == 'socket.io') {
      this.connector = new SocketIoConnector(this.options);
    } else if (this.options.broadcaster == 'null') {
      this.connector = new NullConnector(this.options);
    } else if (typeof this.options.broadcaster == 'function') {
      this.connector = new this.options.broadcaster(this.options);
    }
  };
  /**
   * Disconnect from the Echo server.
   */
  Echo.prototype.disconnect = function () {
    this.connector.disconnect();
  };
  /**
   * Get a presence channel instance by name.
   */
  Echo.prototype.join = function (channel) {
    return this.connector.presenceChannel(channel);
  };
  /**
   * Leave the given channel, as well as its private and presence variants.
   */
  Echo.prototype.leave = function (channel) {
    this.connector.leave(channel);
  };
  /**
   * Leave the given channel.
   */
  Echo.prototype.leaveChannel = function (channel) {
    this.connector.leaveChannel(channel);
  };
  /**
   * Leave all channels.
   */
  Echo.prototype.leaveAllChannels = function () {
    for (var channel in this.connector.channels) {
      this.leaveChannel(channel);
    }
  };
  /**
   * Listen for an event on a channel instance.
   */
  Echo.prototype.listen = function (channel, event, callback) {
    return this.connector.listen(channel, event, callback);
  };
  /**
   * Get a private channel instance by name.
   */
  Echo.prototype["private"] = function (channel) {
    return this.connector.privateChannel(channel);
  };
  /**
   * Get a private encrypted channel instance by name.
   */
  Echo.prototype.encryptedPrivate = function (channel) {
    return this.connector.encryptedPrivateChannel(channel);
  };
  /**
   * Get the Socket ID for the connection.
   */
  Echo.prototype.socketId = function () {
    return this.connector.socketId();
  };
  /**
   * Register 3rd party request interceptiors. These are used to automatically
   * send a connections socket id to a Laravel app with a X-Socket-Id header.
   */
  Echo.prototype.registerInterceptors = function () {
    if (typeof Vue === 'function' && Vue.http) {
      this.registerVueRequestInterceptor();
    }
    if (typeof axios === 'function') {
      this.registerAxiosRequestInterceptor();
    }
    if (typeof jQuery === 'function') {
      this.registerjQueryAjaxSetup();
    }
    if ((typeof Turbo === "undefined" ? "undefined" : _typeof(Turbo)) === 'object') {
      this.registerTurboRequestInterceptor();
    }
  };
  /**
   * Register a Vue HTTP interceptor to add the X-Socket-ID header.
   */
  Echo.prototype.registerVueRequestInterceptor = function () {
    var _this = this;
    Vue.http.interceptors.push(function (request, next) {
      if (_this.socketId()) {
        request.headers.set('X-Socket-ID', _this.socketId());
      }
      next();
    });
  };
  /**
   * Register an Axios HTTP interceptor to add the X-Socket-ID header.
   */
  Echo.prototype.registerAxiosRequestInterceptor = function () {
    var _this = this;
    axios.interceptors.request.use(function (config) {
      if (_this.socketId()) {
        config.headers['X-Socket-Id'] = _this.socketId();
      }
      return config;
    });
  };
  /**
   * Register jQuery AjaxPrefilter to add the X-Socket-ID header.
   */
  Echo.prototype.registerjQueryAjaxSetup = function () {
    var _this = this;
    if (typeof jQuery.ajax != 'undefined') {
      jQuery.ajaxPrefilter(function (options, originalOptions, xhr) {
        if (_this.socketId()) {
          xhr.setRequestHeader('X-Socket-Id', _this.socketId());
        }
      });
    }
  };
  /**
   * Register the Turbo Request interceptor to add the X-Socket-ID header.
   */
  Echo.prototype.registerTurboRequestInterceptor = function () {
    var _this = this;
    document.addEventListener('turbo:before-fetch-request', function (event) {
      event.detail.fetchOptions.headers['X-Socket-Id'] = _this.socketId();
    });
  };
  return Echo;
}();

export { Channel, Connector, EventFormatter, Echo as default };
